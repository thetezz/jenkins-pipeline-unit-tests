import org.junit.*

import com.lesfurets.jenkins.unit.*


class ErblibsTest extends BasePipelineTest {
	def a_script
	final aStringOf100Chars =
	"1234567890223456789032345678904234567890523456789062345678907234567890823456789092345678900234567890"

    @Before
    void setup() {
        super.setUp()
		a_script = loadScript('erblibs/vars/utils.groovy')

    }
	
	@Test
	void testIsStringBlacklisted() {
		def blacklist = ["|", "sudo", "/", "\"", "\\", "\'"]
		
		def result = a_script.isStringBlacklisted("111_222_333_aaa_bbb_ccc", blacklist)
		assert result == false
		
		result = a_script.isStringBlacklisted("playbook.yml | rm -rf *", blacklist)
		assert result == true
		
		result = a_script.isStringBlacklisted("\"playbook.yml\"", blacklist)
		assert result == true
	}
	
    @Test
    void testIsValidLength() {
        def result = a_script.isValidLength("a valid string", 100)
        assert result == true
		result = a_script.isValidLength(aStringOf100Chars, 100)
		assert result == true
		def aStringOf101Chars = aStringOf100Chars + "x"
		result = a_script.isValidLength(aStringOf101Chars, 100)
		assert result == false
    }
	@Test
	void testHasValidExtension() {
		def extensions = [".yml", ".txt"]
		def result = a_script.hasValidExtension("hello.yml", extensions)
		assert result == true
		result = a_script.hasValidExtension("hello.yaml", extensions)
		assert result == false
	
	}
	
	@Test
	void testValidateStringParameter() {
		def errorThrown
		def aStringOf101Chars = aStringOf100Chars + "x"
		
		helper.registerAllowedMethod("error", [ String.class ]) { 
			errorThrown=true
		}
		
		//Tests for type = PLAYBOOK_NAME
		def result = a_script.validateStringParameter("a_valid_playbook_name.yml", "PLAYBOOK_NAME")
	    assert result == "a_valid_playbook_name.yml"
	    assert errorThrown == null
		
		a_script.validateStringParameter("playbook_wth_no_extension", "PLAYBOOK_NAME")
		assert errorThrown == true
		
		errorThrown = null
		a_script.validateStringParameter(aStringOf101Chars, "PLAYBOOK_NAME")
		assert errorThrown == true
		
		errorThrown = null
		a_script.validateStringParameter("attempt_shell_injection | rm -rf /", "PLAYBOOK_NAME")
		assert errorThrown == true
		
		//Tests for type = RELEASE_BRANCH
		errorThrown = null
		result = a_script.validateStringParameter("development", "RELEASE_BRANCH")
		assert result == "development"
		assert errorThrown == null
		
		a_script.validateStringParameter(aStringOf101Chars, "RELEASE_BRANCH")
		assert errorThrown == true
		
		errorThrown = null
		a_script.validateStringParameter("attempt_shell_injection | rm -rf /", "RELEASE_BRANCH")
		assert errorThrown == true
		
		//Test for invalid parameter type
		errorThrown = null
		a_script.validateStringParameter("a_valid_playbook_name.yml", "invalid_parameter_type")
		assert errorThrown == true
	}
}
